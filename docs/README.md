# Python application template

[![][badge-python]][python-docs]
[![][badge-version]][repository-latest-release]

[![][badge-mit]][license-mit]
[![][badge-black]][pip-black]

[![][badge-ci-status]][repository-master]
<!-- [![][badge-ci-coverage]][repository-master] -->
[![][badge-ci-security]][repository-security]
[![][badge-codecov]][repository-codecov]

*Serves as a self-contained, documented template for Python application development.*

**Features:**

* Package management and with [Poetry], which features
    * Dependency management
    * Package publishing &mdash; easy to make it pip-installable  
* [Pre-commit] hooks that
    * Automate code styling with [Black][pip-black]
    * Update the [README]'s table of contents
* [GitLab]-powered [continuous integration][gitlab-ci], featuring
    * [Pytest][python-pytest]-based test suite with [Codecov][codecov]-powered [coverage reports][repository-codecov]
    * [Security verification][repository-security] of Python dependencies (via [GitLab Dependency Scanning][gitlab-dependency-scanning])
    * [License management][repository-licenses] of dependency licenses (via [GitLab License Management][gitlab-license-management])
    * Builds the [Sphinx][python-sphinx]-based documentation and deploys it to https://pedroasad.com.gitlab.io/templates/python/python-scibench/ (via [GitLab pages][gitlab-pages])
    * Uploads successfully built, tagged distributions to [TestPyPI] (on https://test.pypi.org/project/pedroasad-template-python-scibench)
* Version bumping/tagging with [bumpversion][pip-bumpversion]

For a full description of the repository contents (files and directories), refer to the [MANIFEST], and for the history
of changes, refer to the [CHANGELOG].

---

**Table of contents:**

[](TOC)

- [Python application template](#python-scibenchlication-template)
  - [Usage](#usage)
    - [System requirements](#system-requirements)
    - [Development setup](#development-setup)
    - [Building the documentation](#building-the-documentation)
    - [Running the tests](#running-the-tests)
    - [Extending the template](#extending-the-template)
    - [Publishing on PyPI or TestPyPI](#publishing-on-pypi-or-testpypi)
  - [Further references](#further-references)
  - [Future directions](#future-directions)

[](TOC)

---

## Usage
### System requirements

* Python 3.6
* [Poetry] 0.12


### Development setup

1. Create a [virtual environment][virtualenv] and install dependencies. Two of the most common options are:

    * Letting [Poetry] create the virtual environment and install dependencies for you (the default if
      you have just installed Poetry):
      
      ```bash
      poetry install  # This will create a virtual environment in a new subdirectory 
                      # of $(poetry config settings.virtualenvs.path) 
      ```
      
      An interesting variation is telling Poetry to create the environment inside the project's root folder by running
      
      ```bash
      poetry config settings.virtualenvs.in-project true
      ```
      
      before the `install` command.
      
    * If you have choosen to turn off automatic environment creation using
    
      ```bash
      poetry config settings.virtualenvs.create false
      ```

      then the following steps should suffice:
      
      ```bash
      virtualenv -p $(which python3.7) .venv
      source venv/bin/activate.sh
      poetry install
      ```
      
      In this case, keep in mind that you need to activate the environment using the `source` command before issuing any
      Poetry commands. You may choose a name for your virtual environment other than `.venv`, but this one is already on
      [`.gitignore`](.gitignore). 
      
1. **(Optional)** Set up [pre-commit][pre-commit]. It is already included as *dev*-dependency in
   [pyproject.toml](pyproject.toml) and a [.pre-commit-config.yaml](.pre-commit-config.yaml) file is provided. Hence,
   you just need:
   
   ```bash
   pre-commit install
   ```


### Building the documentation

Provided that the virtual environment is active, and dependencies have been installed, it is as simple as

```bash
make html
```

and the output will be placed under `docs/_build/html`.

The [CI/CD][gitlab-ci] pipeline, through [GitLab Pages][gitlab-pages], will automatically deploy the HTML tree to
https://pedroasad.com.gitlab.io/templates/python/python-scibench/. If you clone the repository and do not make further
changes, documentation built after `git push` will most likely end up in
https://user-or-group-name.gitlab.io/namespace-if-exists/repository-name.
   
   
### Running the tests

Provided that the virtual environment is active, and dependencies have been installed, it is as simple as

```bash
pytest
```

This already produces a code coverage report for the [`app`](app) package, stores it in a `.coveragerc` file and prints
it.


### Extending the template

Once you get your hands on this template, in order to start developing an actual application or library, you'll need to
change a few things. The template's [CI/CD][gitlab-ci] configuration is fully functional and does not depend on any
variables defined in the repository's CI/CD settings page. Hence, to get started, you must:

1. Either
    * fork the repository, or
    * clone it, then set the `origin` remote's URL to your newly created [GitLab][gitlab] repository's URL.
1. Drop any tags from the repository (also remember to alter the [CHANGELOG](CHANGELOG.md) appropriately) with
   ```bash
   git tag | xargs git push --delete origin
   git tag | xargs git tag -d
   ```
1. Check you have the system requirements specified in [Application usage](#application-usage).
1. Create a [virtual environment][virtualenv].
1. Check the [MANIFEST] to see what you'll have to change. Probably, you should start by changing occurrences of the 
   repository URL and project name, *e.g.:*

   ```bash
   OLDURL="https:\/\/gitlab.com\/pedroasad.com\/templates\/python\/python-scibench"
   NEWURL="YOUR BACKSLASH-ESCAPED URL HERE"
   sed -Ei "s/$OLDURL/$NEWURL/g" $(find -type f)
   
   OLDNAME="pedroasad-template-python-scibench"
   NEWNAME="YOUR PROJECT NAME HERE"
   sed -Ei "s/$OLDNAME/$NEWNAME/g" $(find -type f)  
   ```


### Publishing on PyPI or TestPyPI

You can easily publish your package to [PyPI] or [TestPyPI] using `poetry publish`, or [twine][python-twine]. For
instance (assuming you already have registered for an account on [TestPyPI]), you can 

```bash
poetry config repositories.testpypi https://test.pypi.org/legacy
poetry config http-basic.testpypi TESTPYPI_USERNAME TESTPYPI_PASSWORD
```

just once; then, for every version you want to publish, run

```bash
poetry build && poetry publish -r testpypi # or
poetry publish --build -r pypi             # or
```

Alternatively, if you want to use [twine][python-twine], you can install it with `pip install twine`, then

```bash
poetry build && twine upload --repository-url https://test.pypi.org dist/*
```

which can also be made more usable by creating a `.pypirc` file. After running `poetry build`, you can check the
distribution files are ok by running `tar tvzf dist/*.tgz` and inspecting the output. If you do not configure [Poetry]
for [TestPyPI] or another repository using the commands like the ones above, it will publish to default to [PyPI]. 

**Important:** You can test you project is pip-installable with

```bash
pip install --index-url http://test.pypi.org/simple   \
            --extra-index-url http://pypi.org/simple  \
            pedroasad-template-python-scibench             # Replace this with your package's name 

```

**Note:** this repository's [CI config](.gitlab-ci.yml) publishes successful builds on [TestPyPI] on tag pushes since
[version 0.3.0](https://test.pypi.org/project/pedroasad-template-python-scibench/0.3.0/). If you are going to use a similar
setup, set up CI variables for `TESTPYPI_USERNAME` and `TESTPYPI_PASSWORD`, or hard-code them (not recommended, for
security reasons) directly in the [`.gitlab-ci.yml`](.gitlab-ci.yml) script.

   
## Further references

* [Current State of Python Packaging](https://stefanoborini.com/current-status-of-python-packaging/)
* [How to package your Python code](https://python-packaging.readthedocs.io/en/latest/index.html)
   
   
## Future directions

* Add useful Python tasks (via [pyinvoke][python-pyinvoke]) and more [pre-commit][pre-commit] hooks
* Detail the [Sphinx][python-sphinx]-based docs
    * Use [recommonmark](https://www.sphinx-doc.org/en/master/usage/markdown.html) for including the [README](README.md)
* Set up Tox test suite
* Set up Docker deployment
* Integrate Docker deployment into CI
* Add a best practices and contribution guides

---

*&mdash; Powered by [GitLab CI][gitlab-ci]*  
*&mdash; Based on [this blog post][python-setup-post]*

[CHANGELOG]: https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/master/CHANGELOG.md
[MANIFEST]: https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/master/MANIFEST.md
[README]:  https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/master/README.md
[Poetry]: https://github.com/sdispater/poetry
[PyPI]: https://pypi.org
[TestPyPI]: https://test.pypi.org
[badge-black]: https://img.shields.io/badge/code%20style-Black-black.svg
[badge-ci-coverage]: https://gitlab.com/pedroasad.com/templates/python/python-scibench/badges/master/coverage.svg
[badge-ci-security]: https://img.shields.io/badge/security-Check%20here!-yellow.svg
[badge-ci-status]: https://gitlab.com/pedroasad.com/templates/python/python-scibench/badges/master/pipeline.svg
[badge-codecov]: https://codecov.io/gl/pedroasad.com:templates:python/python-scibench/branch/master/graph/badge.svg
[badge-mit]: https://img.shields.io/badge/license-MIT-blue.svg
[badge-python]: https://img.shields.io/badge/Python-%E2%89%A53.6-blue.svg
[badge-version]: https://img.shields.io/badge/version-0.3.0%20(alpha)-orange.svg
[codecov]: https://codecov.io
[gitlab-ci]: https://docs.gitlab.com/ee/ci
[gitlab-dependency-scanning]: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/index.html
[gitlab-license-management]: https://docs.gitlab.com/ee/user/application_security/license_management/index.html
[gitlab-pages]: https://docs.gitlab.com/ee/user/project/pages/
[gitlab]: https://gitlab.com
[license-mit]: https://opensource.org/licenses/MIT
[pip-black]: https://pypi.org/project/black/
[pip-bumpversion]: https://pypi.org/project/bumpversion/
[pre-commit]: https://pre-commit.com/
[python-docs]: https://docs.python.org/3.6/
[python-pyinvoke]: http://www.pyinvoke.org/
[python-pytest]: https://pytest.org/
[python-setup-post]: https://towardsdatascience.com/10-steps-to-set-up-your-python-project-for-success-14ff88b5d13
[python-sphinx]: https://www.sphinx-doc.org/en/master/index.html
[python-twine]: https://twine.readthedocs.io
[repository-codecov]: https://codecov.io/gl/pedroasad.com:templates:python/python-scibench
[repository-latest-release]: https://test.pypi.org/project/pedroasad-template-python-scibench/0.3.0/
[repository-licenses]: https://gitlab.com/pedroasad.com/templates/python/python-scibench/licenses/dashboard
[repository-master]: https://gitlab.com/pedroasad.com/templates/python/python-scibench
[repository-security]: https://gitlab.com/pedroasad.com/templates/python/python-scibench/security/dashboard
[repository]: https://gitlab.com/pedroasad.com/templates/python/python-scibench
[virtualenv]: https://virtualenv.pypa.io/en/latest/
