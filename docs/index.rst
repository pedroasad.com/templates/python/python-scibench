.. python-scibench documentation master file, created by
   sphinx-quickstart on Mon May 13 19:13:38 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to python-scibench's documentation!
===========================================

.. include:: /links.rst

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   README <README>
   CHANGELOG
   MANIFEST

Automatically generated documentation for `Pedro Asad <pedroasad_>`_'s scientific Python 3.6 `project template
<python-scibench_>`_ (forked off python-scibench_), version |version|.


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

