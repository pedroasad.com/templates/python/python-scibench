# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog][keep-a-changelog] and this project adheres to [Semantic
Versioning][semantic-versioning].


## [Unreleased]
### Added

* Pre-configured [Intersphinx](http://www.sphinx-doc.org/en/master/usage/extensions/intersphinx.html) mapping for Python, matplotlib, numpy, and scipy.

## [0.4.1] - 2019-06-24
### Changed

* Distribution uploads are now managed via [twine][python-twine] in the [CI pipeline](.gitlab-ci.yml).

## [0.4.0] - 2019-06-24
### Added

* [`MANIFEST.md`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.4.0/MANIFEST.md) file, which is now pointed by the [README]'s introduction.
* Additional metadata in [`pyproject.toml`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.4.0/pyproject.toml) (documentation, website, README, etc.).
* Package successfully built by the [CI pipeline](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.4.0/.gitlab-ci.yml) are pushed to [TestPyPI].

### Changed 

* Typo in [`pyproject.toml`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.4.0/pyproject.toml).
* Package name is now `pedroasad-template-python-scibench`.
* [README] is now included in the documentation's landing page.

## [0.3.0] - 2019-05-17
### Added

* [`docker`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.3.0/docker) &mdash; Directory tree containing base [Docker][docker] images, with `FROM` statements for different `python:3.*-*` versions.

### Changed

* Source-code directory renamed to `scibench`.
* Nearly fixed [License management][repository-licenses] job in CI (incompatible Python version prevents full fix).
* Using own `3.7-slim` image from [registry] instead of `python:3.7-alpine` image for CI jobs.
* Added base scientific Python packages as dependencies in [`pyproject.toml`].
* Forked [python-app][forked-repository] and changed project name to `python-scibench`.
* [Codecov][codecov] token used in pipeline now comes from protected variable.
* Documentation is only published to https://pedroasad.com.gitlab.io/templates/python/python-scibench on tag pushes.

## [0.2.0] - 2019-05-14
### Added

* [Sphinx][python-sphinx]-based documentation setup, which includes:
    * The [`docs/conf.py`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.2.0/docs/conf.py) configuration file,
    * The template [`docs/index.rst`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.2.0/docs/index.rst) file, and
    * A GNU [`Makefile`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.2.0/docs/Makefile).
    
### Changed

* [pre-commit][pre-commit] configuraiton now uses a hook that fixes the [README](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.2.0/README.md)'s TOC.
* [CI config](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.2.0/.gitlab-ci.yml) is now based on `python:3.6-slim`.

## [0.1.0] - 2019-05-13
### Added

* [`.bumpversion.cfg`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.1.0/.bumpversion.cfg) &mdash; Configuration file for the [bumpversion][bumpversion] version-tagging package.
* [`.coveragerc`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.1.0/.coveragerc) &mdash; Configuration file for the [Coverage.py][python-coverage] reporting tool.
* [`.gitignore`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.1.0/.gitignore) &mdash; List of files and directories paths/patterns [ignored by Git][gitignore].
* [`.gitlab-ci.yml`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.1.0/.gitlab-ci.yml) &mdash; Continuous integration/deploy configuration ([GitLab CI][gitlab-ci]), featuring:
    * [Security verification][repository-security] of Python dependencies (via [Gitlab Dependency Scanning][gitlab-dependency-scanning])
    * [License management][repository-licenses] of dependency licenses (via [Gitlab License Management][gitlab-license-management])
* [`.pre-commit-config.yaml`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.1.0/.pre-commit-config.yaml) &mdash; Configuration file for the [pre-commit][pre-commit] package, which aids in applying useful [Git Hooks][git-hooks] in team workflows. Includes:
    * Automatic code styling with [Black][pip-black].
* [`CHANGELOG.md`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.1.0/CHANGELOG.md) &mdash; this very history file, which follows the [Keep a Changelog][keep-a-changelog] standard.
* [`LICENSE.txt`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.1.0/LICENSE.txt) &mdash; Copy of the [MIT license][license-mit] (a permissive [open source license][open-source]).
* [`README.md`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.1.0/README.md) &mdash; repository front-page.
* [`app`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.1.0/app) &mdash; Base directory of the example Python package distributed by this repository.
* [`poetry.lock`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.1.0/poetry.lock) &mdash; [Poetry][python-poetry]'s resolved dependency file. Whereas [`pyproject.toml`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.1.0/pyproject.toml) specifies [*as abstract as possible*][post-setup-vs-requirements] dependencies, this file 
* [`pyproject.toml`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.1.0/pyproject.toml) &mdash; [PEP-517][pep-517]-compliant packaging metadata, configured with the [Poetry][python-poetry] system. Includes, among other information: package qualifiers, version, author, and all of its dependencies. This file replaces the classic [setup.py file][setup.py] found in *classical* Python packaging.
* [`pytest.ini`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.1.0/pytest.ini) &mdash; Configuration file for the [pytest][python-pytest] Python testing framework.
    * Configured for producing coverage reports (through the [pytest-cov][pip-pytest-cov] plugin).
    * Features [Codecov][codecov]-powered [coverage reports][repository-codecov]
* [`tests`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.1.0/tests) &mdash; [pytest][python-pytest]-powered test-suite.

[Unreleased]: https://gitlab.com/pedroasad.com/templates/python/python-scibench/compare?from=master&to=release
[0.4.1]: https://gitlab.com/pedroasad.com/templates/python/python-scibench/compare?from=0.4.0&to=0.4.1
[0.4.0]: https://gitlab.com/pedroasad.com/templates/python/python-scibench/compare?from=0.3.0&to=0.4.0
[0.3.0]: https://gitlab.com/pedroasad.com/templates/python/python-scibench/compare?from=0.2.0&to=0.3.0
[0.2.0]: https://gitlab.com/pedroasad.com/templates/python/python-scibench/compare?from=0.1.0&to=0.2.0
[0.1.0]: https://gitlab.com/pedroasad.com/templates/python/python-scibench/tags/0.1.0

[LICENSE]: https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/master/LICENSE.txt
[MANIFEST]: https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/master/MANIFEST.md
[README]: https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/master/README.md

[bumpversion]: https://github.com/peritus/bumpversion
[codecov]: https://codecov.io
[forked-repository]: https://gitlab.com/pedroasad.com/templates/python/python-app
[git-hooks]: https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks
[gitignore]: https://git-scm.com/docs/gitignore 
[gitlab-ci]: https://docs.gitlab.com/ee/ci
[gitlab-dependency-scanning]: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/index.html
[gitlab-license-management]: https://docs.gitlab.com/ee/user/application_security/license_management/index.html
[gnu-makefile]: https://www.gnu.org/software/make/manual/make.html
[keep-a-changelog]: https://keepachangelog.com/en/1.0.0/
[license-mit]: https://opensource.org/licenses/MIT
[open-source]: https://opensource.org/
[pep-517]: https://www.python.org/dev/peps/pep-0517/
[pip-black]: https://pypi.org/project/black/
[pip-pytest-cov]: https://pypi.org/project/pytest-cov/
[post-setup-vs-requirements]: https://caremad.io/posts/2013/07/setup-vs-requirement/
[pre-commit]: https://pre-commit.com/
[python-coverage]: https://coverage.readthedocs.io
[python-poetry]: https://github.com/sdispater/poetry
[python-pytest]: https://pytest.org/
[python-sphinx]: https://www.sphinx-doc.org/en/master/index.html
[python-twine]: https://twine.readthedocs.io
[repository-codecov]: https://codecov.io/gl/pedroasad.com:templates:python/python-scibench
[repository-licenses]: https://gitlab.com/pedroasad.com/templates/python/python-scibench/licenses/dashboard
[repository-security]: https://gitlab.com/pedroasad.com/templates/python/python-scibench/security/dashboard
[semantic-versioning]: https://semver.org/spec/v2.0.0.html
[setup.py]: https://docs.python.org/3.7/distutils/setupscript.html
