Manifest
========

* [`.bumpversion.cfg`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.4.1/.bumpversion.cfg) &mdash; Configuration file for the [bumpversion][bumpversion] version-tagging package.
* [`.coveragerc`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.4.1/.coveragerc) &mdash; Configuration file for the [Coverage.py][python-coverage] reporting tool.
* [`.gitignore`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.4.1/.gitignore) &mdash; List of files and directories paths/patterns [ignored by Git][gitignore].
* [`.gitlab-ci.yml`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.4.1/.gitlab-ci.yml) &mdash; Continuous integration/deploy configuration ([GitLab CI][gitlab-ci]).
* [`.pre-commit-config.yaml`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.4.1/.pre-commit-config.yaml) &mdash; Configuration file for the [pre-commit][pre-commit] package, which aids in applying useful [Git Hooks][git-hooks] in team workflows. 
* [`CHANGELOG.md`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.4.1/CHANGELOG.md) &mdash; organized history of changes to this repository, following the [Keep a Changelog][keep-a-changelog] standard.
* [`MANIFEST.md`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.4.1/MANIFEST.md) &mdash; this own file, with a description of the repository's contents.
* [`LICENSE.txt`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.4.1/LICENSE.txt) &mdash; Copy of the [MIT license][license-mit] (a permissive [open source license][open-source]).
* [`README.md`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.4.1/README.md) &mdash; Source file for this very front-page!
* [`app`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.4.1/app) &mdash; Base directory of the example Python package distributed by this repository.
* [`docs`]() &mdash; Root of [Sphinx][python-sphinx]-based documentation (built, preferrably, via the [`Makefile`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.4.1/docs/Makefile)).
    * [`Makefile`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.4.1/docs/Makefile)  &mdash; [GNU Makefile][gnu-makefile] for building the [Sphinx][python-sphinx]-based documentation.
* [`poetry.lock`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.4.1/poetry.lock) &mdash; [Poetry][python-poetry]'s resolved dependency file. Whereas [`pyproject.toml`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.4.1/pyproject.toml) specifies [*as abstract as possible*][post-setup-vs-requirements] dependencies, this file 
* [`pyproject.toml`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.4.1/pyproject.toml) &mdash; [PEP-517][pep-517]-compliant packaging metadata, configured with the [Poetry][python-poetry] system. Includes, among other information: package qualifiers, version, author, and all of its dependencies. This file replaces the classic [setup.py file][setup.py] found in *classical* Python packaging.
* [`pytest.ini`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.4.1/pytest.ini) &mdash; Configuration file for the [pytest][python-pytest] Python testing framework. It is currently configured for producing coverage reports (through the [pytest-cov][pip-pytest-cov] plugin).
* [`tests`](https://gitlab.com/pedroasad.com/templates/python/python-scibench/blob/0.4.1/tests) &mdash; [pytest][python-pytest]-powered test-suite.


[bumpversion]: https://github.com/peritus/bumpversion
[git-hooks]: https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks
[gitignore]: https://git-scm.com/docs/gitignore 
[gitlab-ci]: https://docs.gitlab.com/ee/ci
[gnu-makefile]: https://www.gnu.org/software/make/manual/make.html
[keep-a-changelog]: https://keepachangelog.com/en/1.0.0/
[license-mit]: https://opensource.org/licenses/MIT
[open-source]: https://opensource.org/
[pep-517]: https://www.python.org/dev/peps/pep-0517/
[pip-pytest-cov]: https://pypi.org/project/pytest-cov/
[post-setup-vs-requirements]: https://caremad.io/posts/2013/07/setup-vs-requirement/
[pre-commit]: https://pre-commit.com/
[python-coverage]: https://coverage.readthedocs.io
[python-poetry]: https://github.com/sdispater/poetry
[python-pytest]: https://pytest.org/
[python-sphinx]: https://www.sphinx-doc.org/en/master/index.html
[setup.py]: https://docs.python.org/3.7/distutils/setupscript.html
