import scibench


def test_answer():
    assert scibench.answer() == 42


def test_version():
    assert scibench.__version__ == "0.4.1"
